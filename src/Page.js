import React from 'react'
import Button from './Button'
import Counter from './Counter'

export default function Page () {
  return (
    <div>
      <h1>Page Component</h1>
      <Counter/>
      <Button/>
    </div>
  )
}